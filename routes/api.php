<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*Route::post('/location/create', 'LocationController@create');
Route::get('/location/edit/{id}', 'LocationController@edit');
Route::post('/location/update/{id}', 'LocationController@update');
Route::delete('/location/delete/{id}', 'LocationController@delete');
Route::get('/locations', 'LocationController@index');*/

//Route::post('/location_info', 'LocationController@store');
Route::get('/get-location-info/{id}', 'LocationInfoController@show');

Route::get('/get-location/{id}', 'LocationController@show');
Route::post('/save-location-history/{id}', 'LocationController@store');
//Route::get('/save-location-history/{id}', 'LocationController@store');
Route::resource('/locations', 'LocationController', [
  'except' => ['edit','store']
]);

Route::resource('/location-info', 'LocationInfoController', [
  'except' => ['edit']
]);