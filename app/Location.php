<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    //
    protected $fillable = [
    'name',
    'location_type',
    'location_street_address',
    'location_city',
    'location_state',
    'location_zip_code',
    'location_country'
  ];

  /**
  *Get all the tracking data for this location
  */
  public function history()
  {
    return $this->hasMany('App\LocationInfo');
  }
}
