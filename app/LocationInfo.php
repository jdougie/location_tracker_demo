<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationInfo extends Model
{
    //
    protected $fillable = [
    'location_id',
    'info_type',
    'info_texts'
    
  ];
}
