<?php

namespace App\Http\Controllers;

use App\LocationInfo;
use App\Http\Resources\LocationInfoCollection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Faker\Generator;

class LocationInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new LocationInfoCollection(Location_Info::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $info = new LocationInfo([
        'location_id' => $request->get('location_id'),
        'info_type' => $request->get('info_type'),
        'info_texts' => $request->get('info_texts')
          ]);
        $info->save();

        return response()->json('success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        //return all info records for a company
        $info = LocationInfo::whereIn('location_id',[$id])->get();

        return response($info->jsonSerialize(), Response::HTTP_CREATED);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $info = LocationInfo::find($id);

        $info->update($request->all());

        return response()->json('successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        LocationInfo::destroy($id);

        return response(null, Response::HTTP_OK);
    }
}
