<?php

namespace App\Http\Controllers;

use App\Location;
use App\LocationInfo;
use App\Http\Resources\LocationsCollection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Faker\Generator;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return response(Location::all()->jsonSerialize(), Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Generator $faker)
    {
        //
        $location = new Location();
        $location->name = $faker->company;
        $location->location_type = $faker->randomElement($array = array('Business','Landmark','Park','Home'));
        $location->location_street_address = $faker->company;
        $location->location_city = $faker->city;
        $location->location_state = $faker->state;
        $location->location_zip_code = $faker->postcode;
        $location->location_country = $faker->country;

        $location->save();

        return response($location->jsonSerialize(), Response::HTTP_CREATED);
        
    }


    /**
     * Store location history data here
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id,Request $request)
    {
        $data = $request->all();
        //$data = array("info_type"=>"Price Change","info_texts"=>"Increased by $1000");
        $new_history = new LocationInfo([
            'info_type'=>$request->get('info_type'),
            'info_texts' => $request->get('info_texts')]);
   //$new_history = new LocationInfo($data);
        $location = Location::find($id);

        $location->history()->save($new_history);

        return response($location->jsonSerialize(), Response::HTTP_CREATED);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $location = Location::find($id);

        if(!$location)
            return response()->json(['message' => 'Couldnot find the location!'],422);
        //$location_history = array("info_type"=>"Price","info_text"=>"nonsense");
        $location_history = $location->history()->get();
        $package = array("location" => $location, "history" =>$location_history );
        //return response($package->jsonSerialize(), Response::HTTP_CREATED);
        return response($package, Response::HTTP_CREATED);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $location = Location::findOrFail($id);
        $location->name = $request->name;
        $location->save();

        return response(null, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $location = Location::find($id);

        if(!$location)
            return response()->json(['message' => 'Couldnot find the location!'],422);

        $location->delete();

        return response(null, Response::HTTP_OK);
    }
}
